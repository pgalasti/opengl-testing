#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include<stdio.h>
#include<string.h>
#include<iostream>

#ifdef __LINUX__
#include <libgen.h>
#endif

#include "include/GL/glew.h"
#include "include/GL/freeglut.h"
#include "include/ogldev_util.h"
#include "include/ogldev_math_3d.h"

#define RETURN_SUCCESS 0
#define RETURN_FAILURE 1

GLuint VBO;

const char* pszVSFileName = "shader.vs";
const char* pszFSFileName = "shader.fs";
char pszVSFilePath[1024];
char pszFSFilePath[1024];

static void RenderSceneCB();
static void InitializeGlutCallbacks();
static void CreateVertexBuffer();
static void AddShader(GLuint ShaderProgram, const char* pszShaderText, GLenum ShaderType);
static void CompileShaders();

int main(int argc, char** argv)
{
	memset(pszVSFilePath, 0, 1024);
	memset(pszFSFilePath, 0, 1024);

#ifdef _WIN32
	char szDrive[_MAX_DRIVE];
	char szDirectory[_MAX_DIR];
	_splitpath(argv[0], szDrive, szDirectory, nullptr, nullptr);
	sprintf(pszVSFilePath, "%s\\%s\\%s", szDrive, szDirectory, pszVSFileName);
	sprintf(pszFSFilePath, "%s\\%s\\%s", szDrive, szDirectory, pszFSFileName);
#else
	char* pszDirectory = basename(argv[0]);
	sprintf("%s/%s", szDirectory, pszVSFileName);
	sprintf("%s/%s", szDirectory, pszFSFilePath);
#endif
	
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA);
    glutInitWindowSize(1024, 768);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Tutorial 04");

    InitializeGlutCallbacks();

    GLenum res = glewInit();
    if(res != GLEW_OK)
    {
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return RETURN_FAILURE;
    }

    printf("GL Version: %s\n", glGetString(GL_VERSION));

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    CreateVertexBuffer();

    CompileShaders();

    glutMainLoop();

    return RETURN_SUCCESS;
}

static void RenderSceneCB()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);

    glutSwapBuffers();
}

static void InitializeGlutCallbacks()
{
    glutDisplayFunc(RenderSceneCB);
}

static void CreateVertexBuffer()
{
    Vector3f Vertices[3];
    Vertices[0] = Vector3f(-1.0f, -1.0f, 0.0f);
    Vertices[1] = Vector3f(1.0f, -1.0f, 0.0f);
    Vertices[2] = Vector3f(0.0f, 1.0f, 0.0f);

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
}

static void AddShader(GLuint ShaderProgram, const char* pszShaderText, GLenum ShaderType)
{
    GLuint ShaderObj = glCreateShader(ShaderType);

    if(ShaderObj == 0)
    {
        fprintf(stderr, "Error creating shader type %d\n", ShaderType);
        exit(0);
    }

    const GLchar* p[1];
    p[0] = pszShaderText;
    GLint Lengths[1];
    Lengths[0] = strlen(pszShaderText);
    glShaderSource(ShaderObj, 1, p, Lengths);
    glCompileShader(ShaderObj);
    GLint success;
    glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);

    if(!success)
    {
        GLchar InfoLog[1024];
        glGetShaderInfoLog(ShaderObj, 1024, 0, InfoLog);
        fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
        exit(1);
    }

    glAttachShader(ShaderProgram, ShaderObj);
}

static void CompileShaders()
{
    GLuint ShaderProgram = glCreateProgram();

    if(ShaderProgram == 0)
    {
        fprintf(stderr, "Error creating shader program\n");
        exit(1);
    }

    string vs, fs;

	if (!ReadFile(pszVSFilePath, vs))
        exit(1);
	if (!ReadFile(pszFSFilePath, fs))
        exit(1);

    AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);
    AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

    GLint Success = 0;
    GLchar ErrorLog[1024] = { 0 };

    glLinkProgram(ShaderProgram);
    glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

    if(Success == 0)
    {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), 0, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    glValidateProgram(ShaderProgram);
    glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
    if(!Success)
    {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), 0, ErrorLog);
        fprintf(stderr, "Invalid shader Program: '%s'\n", ErrorLog);
        exit(1);
    }

    glUseProgram(ShaderProgram);
}

